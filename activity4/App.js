
import * as React from 'react';
import { ImageBackground, StyleSheet, Text, View, Button } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack=createStackNavigator();

function Home({navigation}){
  const onPressHandler=()=>{
    navigation.navigate("Reason1");
  }
      return(
      <View style={styles.container}>
        <ImageBackground source={require('./assets/img2.png')} resizeMode="center" style={styles.image}>
          <Text style={styles.text}>5 reasons why I chose this course</Text>
          <Button style={styles.button} title="Continue" onPress={onPressHandler}/>
        </ImageBackground>
      </View>
    );
      }
function Screen1({navigation}){
const onPressHandler=()=>{
  navigation.navigate("Reason2");
}
    return(
    <View style={styles.container}>
      <ImageBackground source={require('./assets/img1.png')} resizeMode="center" style={styles.image}>
        <Text style={styles.text}>1) I chose this course because I want to become a professional programmer.</Text>
        <Button style={styles.button} title="Continue" onPress={onPressHandler}/>
      </ImageBackground>
    </View>
  );
    }
  function Screen2({navigation}){
const onPressHandler=()=>{
  navigation.navigate("Reason3");
}
    return(
    <View style={styles.container}>
      <ImageBackground source={require('./assets/img4.png')} resizeMode="center" style={styles.image}>
        <Text style={styles.text}>2) I chose this course because it doesn't need advanced english skills</Text>
        <Button style={styles.button} title="Continue" onPress={onPressHandler}/>
      </ImageBackground>
    </View>
  );
    }
  
  function Screen3({navigation}){
const onPressHandler=()=>{
  navigation.navigate("Reason4");
}
    return(
    <View style={styles.container}>
      <ImageBackground source={require('./assets/img2.png')} resizeMode="center" style={styles.image}>
        <Text style={styles.text}>3) I chose this course because I want to learn how to create programs</Text>
        <Button onPress={onPressHandler} style={styles.button} title="Continue"  />
      </ImageBackground>
    </View>
  );
    }
  function Screen4({navigation}){
const onPressHandler=()=>{
  navigation.navigate("Reason5");
}
    return(
    <View style={styles.container}>
      <ImageBackground source={require('./assets/img3.png')} resizeMode="center" style={styles.image}>
        <Text style={styles.text}>4) I chose this course because I want to learn new things about technology.</Text>
        <Button style={styles.button} title="Continue" onPress={onPressHandler} />
      </ImageBackground>
    </View>
  );
    }
  function Screen5({navigation}){
 const onPressHandler=()=>{
   navigation.navigate("Home");
 }   
    return(
    <View style={styles.container}>
      <ImageBackground source={require('./assets/img5.png')} resizeMode="center" style={styles.image}>
        <Text style={styles.text}>5) I chose this course to enhance my computer skills.</Text>
        <Button style={styles.button} title="Home" onPress={onPressHandler} />
      </ImageBackground>
    </View>
  );
    }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    image: {
      flex: 1,
      justifyContent: "center"
    },
    text: {
      color: "orange",
      fontSize: 42,
      lineHeight: 84,
      fontWeight: "bold",
      textAlign: "center",
      marginBottom: 60,
    },
    button:{
      color: "blue",
      borderRadius: 5,
      borderColor: "black", 
    }
  });

export default function App(){
  return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home}/>
        <Stack.Screen name="Reason1" component={Screen1}options={{header:()=>null}}/>
        <Stack.Screen name="Reason2" component={Screen2}options={{header:()=>null}}/>
        <Stack.Screen name="Reason3" component={Screen3}options={{header:()=>null}}/>
        <Stack.Screen name="Reason4" component={Screen4}options={{header:()=>null}}/>
        <Stack.Screen name="Reason5" component={Screen5}options={{header:()=>null}}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}
