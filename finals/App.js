import React, {useState} from 'react';
import { StyleSheet,
         View,
         Text,
         ImageBackground,
         TextInput,
         TouchableOpacity, 
         Keyboard,
       } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons'; 
import * as Speech from "expo-speech";
export default function App() {
  const [newWord, setNewWord]=useState('');
  const [checkedWord, setCheckedWord]= useState('')
  const [definition, setDefinition] = useState("");
  const [example, setExample] = useState("");

  const searchWord=(enteredWord)=>{
    setNewWord(enteredWord)
  };

  getInfo =()=>{
    var url= "https://api.dictionaryapi.dev/api/v2/entries/en/"+newWord;
    return fetch (url)
    .then((data)=>{
      return data.json();
    })
    .then((response)=>{
      var word=response[0].word
        setCheckedWord(word);

      var def = response[0].meanings[0].definitions[0].definition;
        setDefinition(def);

      var eg = response[0].meanings[0].definitions[0].example;
        setExample(eg);
        
    Keyboard.dismiss()
    });
  };
  const speak = () => {
    Speech.speak(checkedWord);
  };

  const clear = () => {
    setCheckedWord("");
    setDefinition("");
    setExample("");
    setNewWord("");
  };
  return (
     <ImageBackground
     style={{flex:1}}
     resizemode ="cover"
     source={require("./assets/background.jpeg")}
     >
       <View style={styles.textPlacement}>
          <Text style={styles.textStyle}>Dictionary</Text>

       </View>
       <View style={{flex:0.8,}}>
       <View style={styles.inputBox}>
          <TextInput 
          style={styles.textInput}
          placeholder='Search a word'
          onChangeText={searchWord}
          value={newWord}
          clearButtonMode='always'
          />
          
       </View> 
       <View style={styles.buttons}>
        <TouchableOpacity style={styles.buttonStyle} 
            onPress={()=>{getInfo();}}>
            <MaterialIcons name="search" size={40} color="white" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonStyle}
            onPress={()=>{clear();}}>
            <Text style={{fontSize: 30, fontWeight: "bold",color:'white'}}>CLear</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonStyle}
            onPress={()=>{speak();}}>
            <MaterialIcons name="campaign" size={40} color="white" />
        </TouchableOpacity>
       </View>
       <View>
              <Text style={styles.textDesign}>Entered Word : {checkedWord}</Text>
              <Text style={styles.textDesign}>Definition : {definition}</Text>
              <Text style={styles.textDesign}>Example : {example}</Text>
            </View>

       </View>
    
    </ImageBackground> 
  );
}

const styles = StyleSheet.create({

  textStyle:{
    fontSize: 70,
    fontWeight:"bold",
    color:"#232B2B",

  },
  textPlacement:{
    flex: 0.2,
    alignItems:"center",
    marginTop: 30,
  },
  inputBox:{
    alignContent:"center",
    alignItems:"center"
  },
  textInput:{
    height: 40,
    width: "60%",
    borderWidth: 3,
    textAlign: 'center',
    borderColor:'#232B2B',
    backgroundColor: '#F9F6EE',
    flexDirection:'row',
  },
  buttons:{
    alignItems: 'center',
    flexDirection:'row',
    justifyContent:'space-evenly',
    marginTop: 20,
    marginBottom: 20,
  },
  buttonStyle:{
    borderRadius: 10,
    backgroundColor:'#232B2B',
    borderColor:'#232B2B',
    borderWidth: 3,
    
  },
  textDesign:{
    fontSize: 20,
    backgroundColor: "#232B2B",
    borderRadius: 10,
    marginTop: 10,
    alignSelf: "center",
    color:'white',
    borderWidth:3,
    borderColor:'#232B2B',
    textAlign:'center'
  },
});
