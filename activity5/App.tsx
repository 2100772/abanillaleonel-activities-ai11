
import { StyleSheet, View } from 'react-native';
import Buttons from "./components/ButtonPress"
export default function App() {
  return (
    <View style={styles.container}>
      <Buttons/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#1b1b1b',
  },
});
