import {StyleSheet, Text, TouchableOpacity} from "react-native";

interface ButtonProps{
    onPress:()=>void;
    title: string;
}
export default function Button({title, onPress,}:ButtonProps){
    return(
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    );
}
const styles= StyleSheet.create({
container:{alignItems: "center", justifyContent:"center", width:80, height: 80, borderRadius: 10, backgroundColor: '#292929', margin: 5},
text:{fontSize: 50, fontWeight: "bold", color:'white'},
});