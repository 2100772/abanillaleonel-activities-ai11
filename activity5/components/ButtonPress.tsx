import *as React from "react";
import Button from "./CalcButton";
import {View, Text, StyleSheet} from "react-native";
import Button2 from "./CalcButton2"
export default function Buttons(){
const [firstNumber, setFirstNumber] =React.useState("");
const [secondNumber, setSecondNumber] =React.useState("");
const [operation, setOperation] =React.useState("");
const [result, setResult] =React.useState<Number | null>(null);

const handleNumberPress=(buttonValue: string)=>{
    setFirstNumber(firstNumber + buttonValue);
};
const handleOperationPress=(buttonValue: string)=>{
    setOperation(buttonValue);
    setSecondNumber(firstNumber);
    setFirstNumber("");
};
const clear=()=>{
    setFirstNumber("");
    setSecondNumber("");
    setOperation("");
    setResult(null);
};

const getResult=()=>{
    switch(operation){
        case "+":
            clear();
            setResult(parseInt(secondNumber)+ parseInt(firstNumber));
            break;
        case "-":
            clear();
            setResult(parseInt(secondNumber)- parseInt(firstNumber));
            break;
        case "x":
            clear();
            setResult(parseInt(secondNumber)* parseInt(firstNumber));
            break;
        case "÷":
            clear();
            setResult(parseInt(secondNumber)/ parseInt(firstNumber));
            break;
        default:
            clear();
            setResult(0);
            break;
    }
};
const firstNumberDisplay =()=>{
    if (result!==null){
        return <Text style={styles.disp}>{result?.toString()}</Text>;
    }
    if (firstNumber && secondNumber.length<6){
        return <Text style={styles.disp}>{firstNumber}</Text>; 
    }
    if (firstNumber===""){
        return <Text style={styles.disp}>{"0"}</Text>; 

    }
};
return(
    <View style={styles.container}>
        <View style={{height: 120, width: "90%", justifyContent:"flex-end",}}>
            
            <Text style={styles.disp}>
                {secondNumber}
            <Text style={{color: "blue", fontSize:50, fontWeight: "500", }}>{operation}{firstNumberDisplay()}</Text>
            </Text>
            
        </View>
    <View style={styles.buttonRow}>
    <Button2 title='C' onPress={clear}/>
    <Button2 title='+/-' onPress={()=>{handleOperationPress("+/-")}}/>
    <Button2 title='%' onPress={()=>{handleOperationPress("%")}}/>
    <Button2 title="⌫" onPress={()=>setFirstNumber(firstNumber.slice(0,-1))}/>
    </View>
    <View style={styles.buttonRow}>
    <Button title="7" onPress={()=>handleNumberPress("7")}/>
    <Button title="8" onPress={()=>handleNumberPress("8")}/>
    <Button title="9" onPress={()=>handleNumberPress("9")}/>
    <Button2 title='÷' onPress={()=>{handleOperationPress("÷")}}/>
    </View>
    <View style={styles.buttonRow}>
    <Button title="4" onPress={()=>handleNumberPress("4")}/>
    <Button title="5" onPress={()=>handleNumberPress("5")}/>
    <Button title="6" onPress={()=>handleNumberPress("6")}/>
    <Button2 title='x' onPress={()=>{handleOperationPress("x")}}/>
    </View>
    <View style={styles.buttonRow}>
    <Button title="1" onPress={()=>handleNumberPress("1")}/>
    <Button title="2" onPress={()=>handleNumberPress("2")}/>
    <Button title="3" onPress={()=>handleNumberPress("3")}/>
    <Button2 title='-' onPress={()=>{handleOperationPress("-")}}/>
    </View>
    <View style={styles.buttonRow}>
    <Button title="." onPress={()=>handleNumberPress(".")}/>
    <Button title="0" onPress={()=>handleNumberPress("0")}/>
    <Button title='=' onPress={()=>getResult()}/>
    <Button2 title='+' onPress={()=>{handleOperationPress("+")}}/>
    </View>
    </View>
);
}
const styles= StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        backgroundColor: '#1b1b1b',
      },
    buttonRow:{flexDirection:"row", justifycontent: "space-between"},
    disp:{color: "white", fontSize: 50, fontWeight: "bold", justifyContent: 'flex-end',}
});