import * as React from 'react';
import {useState} from 'react';
import { Keyboard, ScrollView, StyleSheet, Text, View,TouchableOpacity} from 'react-native';
import TaskInputField from '../components/TaskInputField';
import TaskItem from '../components/TaskItem';
import { MaterialIcons } from '@expo/vector-icons';


export default function Todo ({ navigation }) {

  const [tasks, setTasks] = useState([]);
  const addTask = (task) => {
    if (task == null)
     return;
    setTasks([...tasks, task]);
    Keyboard.dismiss();
  }
  
  const deleteTask = (deleteIndex) => {
    setTasks(tasks.filter((value, index) => index != deleteIndex));
  }

  return (
    <View style={styles.container}>
        <Text style={styles.heading}>TODO LIST</Text>
          <TouchableOpacity onPress={()=>navigation.navigate('Complete')}>
                    <MaterialIcons name="star" size={40} color='#fff'/>
          </TouchableOpacity>
      <ScrollView style={styles.scrollView}>
        {
        tasks.map((task, index) => {
          return (
            <View key={index} style={styles.taskContainer}>
              <TaskItem task={task} deleteTask={() => deleteTask(index)}/>
            </View>
          );
        })
      }
      </ScrollView>
      <TaskInputField addTask={addTask}/>
    </View>
  );
}   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1E1A3C',
  },
  heading: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '600',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 20,
  },
  scrollView: {
    marginBottom: 70,
  },
  taskContainer: {
    marginTop: 20,
  }
});