import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from 'react-navigation/native';
import Todo from '../screens/todo';
import Complete from '../sceens/complete';

const Stack=createNativeStackNavigator();
export default function App(){
  return(
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Todo" component={Todo}/>
        <Stack.Screen name="Complete" component={Complete}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}