import {createStackNavigator} from 'react-navigation-stack';
import{createAppContainer} from 'react-navigation';

import complete from '../screens/complete';
import App from '../App';

const screen={
    todoScreen:{
        screen: App
    },
    CompleteScreen: {
        screen: complete
    }

}
const HomeStack= createStackNavigator(screen);
export default createAppContainer(HomeStack)